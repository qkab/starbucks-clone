import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'

const Home: NextPage = () => {
  return (
    <div className="h-screen">
      <Head>
        <title>Starbucks Clone</title>
        <meta name="description" content="Starbuck clone for educational purpose" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="flex w-full">
        <section>
          <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veritatis vitae praesentium labore accusamus provident! Possimus ullam minus consectetur est ipsam esse! Nisi corporis culpa non impedit eius nesciunt saepe eveniet.</p>
        </section>
        <section>
          <h1 className="text-3sm font-bold underline">
            Welcome to the Starbucks Clone
          </h1>
        </section>
      </main>

      <footer className="fixed bottom-0">
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className="w-10 h-10">
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  )
}

export default Home
